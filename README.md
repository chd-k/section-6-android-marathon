# Section 6 Android Marathon

Task "Builders"

1. [Function literals with receiver](https://gitlab.com/chd-k/section-6-android-marathon/-/tree/main/Function%20literals%20with%20receiver)
1. [String and map builders](https://gitlab.com/chd-k/section-6-android-marathon/-/tree/main/String%20and%20map%20builders)
1. [The function apply](https://gitlab.com/chd-k/section-6-android-marathon/-/tree/main/The%20function%20apply)
1. [Html builders](https://gitlab.com/chd-k/section-6-android-marathon/-/tree/main/Html%20builders)
1. [Builders how it works](https://gitlab.com/chd-k/section-6-android-marathon/-/tree/main/Builders%20how%20it%20works)
1. [Builders implementation](https://gitlab.com/chd-k/section-6-android-marathon/-/tree/main/Builders%20implementation)

